/*
        ----------------------------------------------------------------------

        --- Caen SpA - Computing Systems Division ---

        a3818.c

        Source file for the CAEN A3818 HS CONET board driver.

        September   2010 : Created.
		November     2013 : Last Release.

        ----------------------------------------------------------------------
*/

#include <assert.h>
#include <string.h>
#include "a3818.h"

#include <stdio.h>
#include <sys/mman.h>
//#include <asm/cachectl.h>
//#include <linux/cacheflush.h>

#define ENABLE_RX_INT(opt_link)\
{\
	writel(A3818_RDYINTDIS, s->baseaddr[opt_link] + A3818_IOCTL_C);\
}

#define DISABLE_RX_INT(opt_link)\
{\
	writel(A3818_RDYINTDIS, s->baseaddr[opt_link] + A3818_IOCTL_S);\
}

/*      ----------------------------------------------------------------------

        Function prototypes

        ----------------------------------------------------------------------
*/

void a3818_handle_rx_pkt(struct a3818_state *s, int opt_link);
void a3818_dispatch_pkt(struct a3818_state *s, int opt_link);

static inline void writel(uint32_t val, void * addr) {
        *((uint32_t*)addr) = val;
}
static inline uint32_t readl(void * addr) {
        return *((uint32_t*)addr);
}

/*
        ----------------------------------------------------------------------

        a3818_send_pkt

        ----------------------------------------------------------------------
*/

static int a3818_send_pkt(struct a3818_state *s, int slave, int opt_link, /*const char *buf,*/ int count) {
	int nlw, i;

	DISABLE_RX_INT(opt_link);

	/* 32-bit word alignment */
	nlw = (count >> 2) + (count & 3 ? 1 : 0) + 1;
	/* Send data without DMA (A3818 doesn't support DMA write facilitie) */
	for( i = 0; i < nlw; i++ ) {
		writel(s->buff_dma_out[opt_link][slave][i], s->baseaddr[opt_link] + A3818_TXFIFO);
		//DPRINTK("Pck[%d] : %x\n", i, s->buff_dma_out[opt_link][slave][i]);
	}
	return count;
}

/*
        ----------------------------------------------------------------------

        a3818_recv_pkt

        ----------------------------------------------------------------------
*/

static int a3818_recv_pkt(struct a3818_state *s, int slave, int opt_link/*, int *status*/) {
	int ret;
	int i;
	uint32_t tr_stat;
	int startDMA = 0;
//	DISABLE_RX_INT(opt_link); //by default
	//DPRINTK("a3818_recv_pkt: DISABLE_RX_INT\n");

//	if( !s->rx_ready[opt_link][slave] && !s->DMAInProgress[opt_link] && !s->LocalInterrupt[opt_link]) {
//	msync(s->baseaddr[opt_link]+A3818_LINK_TRS,4,MS_SYNC);
	if(!s->rx_ready[opt_link][slave]) {
		for( i = 0; i < 15; i++) {
                        asm volatile("": : :"memory"); //needs a barrier here, otherwise trs value never gets correctly updated

			tr_stat = readl(s->baseaddr[opt_link] + A3818_LINK_TRS);

			if( tr_stat == 0xFFFFFFFF ) {
			        assert(0);
				//break;
			}
			else if( tr_stat & 0xFFFF0000 ) {

				s->tr_stat[opt_link] = tr_stat;
				a3818_handle_rx_pkt(s, opt_link/*, 0*/);
				startDMA = 1;
				break;
			}
//			else  printf("%x\n", tr_stat);
		}
	}
	//DPRINTK("rcv-pkt: tr_stat = %x, i= %d\n", tr_stat,i);
	
	assert(startDMA);
	//return 0;

	//if (!startDMA) ENABLE_RX_INT(opt_link);

        //should be true if not waiting for interrupt (unless packet was not returned)
        assert(s->rx_ready[opt_link][slave]);

	ret = s->ndata_app_dma_in[opt_link][slave];
	//DPRINTK("rcv-pkt: read-ret = %d\n", ret );
	//DPRINTK("rcv-pkt: s->rx_status[%d] = %x\n", slave, s->rx_status[opt_link][slave]  );

	return ret;
}

/*
        ----------------------------------------------------------------------

        a3818_handle_rx_pkt

        ----------------------------------------------------------------------
*/
void a3818_handle_rx_pkt(struct a3818_state *s, int opt_link/*, int pp*/)
{
	int DMA_size, i, DMA_size_word;
	u32 mia_var;


	DISABLE_RX_INT(opt_link);//TODO: have INT handling?

	//DPRINTK("a3818_handle_rx_pkt: DISABLE_RX_INT\n");

	/* Check on the length */
	DMA_size_word = (int)(s->tr_stat[opt_link] >> 16);
	DMA_size = DMA_size_word * 4;
//        printf("step2b %d\n",DMA_size_word);fflush(stdout);
	//return 0; // TEST

	//DPRINTK("a3818_handle_rx_pkt: OLDDMA_size = %d\n", DMA_size_word );
	//DPRINTK("a3818_handle_rx_pkt NO DMA: SIZE = %d\n",DMA_size_word);
	for( i = 0; i < DMA_size_word; i++ ){
			mia_var = readl(s->baseaddr[opt_link] + A3818_RXFIFO);
			//DPRINTK("a3818_handle_rx_pkt: i = %d, rx fifo = %x\n",i,mia_var);
			*(u32 *)(s->buff_dma_in[opt_link] + i*4) = mia_var;
  //                      printf("step2c3 READ w at %d\n",(i*4));fflush(stdout);
	}
	s->ndata_dma_in[opt_link] = DMA_size;
	// Dispatch global buffer in per-slave buffers
//        printf("step3\n");fflush(stdout);
	a3818_dispatch_pkt(s, opt_link);
//        printf("step3end\n");fflush(stdout);
	ENABLE_RX_INT(opt_link);
	//DPRINTK("ENABLE_RX_INT\n");
}


/*
        ----------------------------------------------------------------------

        a3818_dispatch_pkt

        ----------------------------------------------------------------------
*/
void a3818_dispatch_pkt(struct a3818_state *s, int opt_link)
{
	int i, pkt_sz, slave, nlw, last_pkt, pos;
	u32  mask = 0;
	char *buff_dma_in = (char*)(s->buff_dma_in[opt_link]);
	char *iobuf;
	int ResidualDMASize = s->ndata_dma_in[opt_link];
	//i is the DMA buffer index with an header of 4 byte
	i = 0;
	do {
        //        printf("step pkt\n");fflush(stdout);
		//DPRINTK("a3818_dispatch_pkt: header %d: %x %x %x %x\n",i,buff_dma_in[i+3],buff_dma_in[i+2],buff_dma_in[i+1],buff_dma_in[i]);
		pkt_sz = (buff_dma_in[i] & 0xFF) + ((buff_dma_in[i+1] << 8) & 0x0100);
		last_pkt = buff_dma_in[i+1] & 0x02;
		i += 3;
		slave = buff_dma_in[i++] & 0xF;
		mask |= 1U << slave;
		nlw = (pkt_sz >> 1) + (pkt_sz & 1);
		//DPRINTK("a3818_dispatch_pkt: Numero di word %d\n", nlw);
		if( last_pkt ) {
			pkt_sz -= 1;    // per togliere lo status
			s->rx_status[opt_link][slave] = (buff_dma_in[i + pkt_sz * 2 - 1] << 8) + buff_dma_in[i + pkt_sz * 2];
			//DPRINTK("a3818_dispatch_pkt: status = %02x slave = %02x\n", s->rx_status[opt_link][slave], slave);
		}
		if( slave >= MAX_V2718 ) {
                        //DPRINTK("a3818_dispatch_pkt: Wrong packet %04x %02x\n", pkt_sz, slave);
			assert(0);
			return;
		}

		// slave buffer
		iobuf = (char*)s->app_dma_in[opt_link][slave];
		// starting position
		pos = s->pos_app_dma_in[opt_link][slave];
		/* Some integrity checks */
		if( (nlw <= (ResidualDMASize / 4)) &&
				(slave < MAX_V2718) && (pkt_sz >= 0) &&
				(pkt_sz <= 0x100) ) {

			// Copy of data ready
          //              printf("step pkt last %d\n",(pkt_sz * 2));fflush(stdout);
			memcpy(&(iobuf[pos]), &(buff_dma_in[i]), (size_t)(pkt_sz * 2));
			ResidualDMASize -= pkt_sz * 2;
			s->ndata_app_dma_in[opt_link][slave] += pkt_sz * 2;
			i += pkt_sz * 2;
			s->pos_app_dma_in[opt_link][slave] += pkt_sz * 2;
			if( last_pkt ) {
				i += 2; // to wipe out the status word
				s->rx_ready[opt_link][slave] = 1;

//? we are in the same thread (no DMA)
//				wake_up_interruptible(&s->read_wait[opt_link][slave]);
                        }
			i += i % 4;  // 32-bit alignment
		}
		else {	
			assert(0);
			//DPRINTK("[%d]disp_pkt: Unhandled packet - slave %d, pkt_sz %d\n", opt_link, slave, pkt_sz);
			break;
		}
	} while( i < s->ndata_dma_in[opt_link] );
	return;
}

/*
        ----------------------------------------------------------------------

        a3818_ioctl

        ----------------------------------------------------------------------
*/
void init_state(struct a3818_state *s,int opt_link, void * baseaddr) {

  int slave = 0;
  //slave = opt_link << 3;

  s->baseaddr[opt_link] = (unsigned char*) baseaddr;
  s->pos_app_dma_in[opt_link][slave] = 0;
  s->rx_ready[opt_link][slave] = 0;
  s->ndata_app_dma_in[opt_link][slave] = 0;
  s->ioctls[opt_link]=0;

}

void ioctl_reg_writel(int opt_link, struct a3818_state *s, uint32_t offset, uint32_t value) {
  writel(value,s->baseaddr[opt_link] + offset);
}

uint32_t ioctl_reg_readl(int opt_link, struct a3818_state *s, uint32_t offset) {
  return readl(s->baseaddr[opt_link] + offset);
}

int ioctl_comm(int opt_link, struct a3818_state *s, struct a3818_comm *comm_in) {
  //int i; // for debug
  int slave = 0;
  int ret = 0;
  //slave = opt_link << 3;

  a3818_comm_t comm;
  //struct a3818_comm_t comm;

  s->ioctls[opt_link]++;

  memcpy(&comm, comm_in, sizeof(comm));
  memcpy(&(s->buff_dma_out[opt_link][slave][1]), comm.out_buf, (size_t)(comm.out_count));

  //TODO: handle link fail RESET?
  assert(!(readl(s->baseaddr[opt_link] + A3818_LINK_SR) & A3818_LINK_FAIL ));


  if( comm.out_count > 0 ) {
	/* Build the header */
	s->buff_dma_out[opt_link][slave][0] = ((uint32_t)slave << 24) | ((uint32_t)slave << 16) | ((comm.out_count >> 1) & 0xFFFF);
	ret = a3818_send_pkt(s, slave, opt_link, /*comm.out_buf,*/ comm.out_count);
	assert(ret>=0);
	if( comm.in_count > 0 ) {
		ret = a3818_recv_pkt(s, slave, opt_link/*, comm.status*/);
//                printf("received1!\n");fflush(stdout);
		assert(ret>=0);
	}
  }
// printf("received 1!\n");fflush(stdout);

  *comm.status = s->rx_status[opt_link][slave];
  assert(comm.in_count >= s->ndata_app_dma_in[opt_link][slave]);
  memcpy(comm.in_buf, s->app_dma_in[opt_link][slave], (size_t)(s->ndata_app_dma_in[opt_link][slave]));
		
  s->pos_app_dma_in[opt_link][slave] = 0;
  s->rx_ready[opt_link][slave] = 0;
  s->ndata_app_dma_in[opt_link][slave] = 0;

  return 0;
}
