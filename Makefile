#LIBS=-lGenericHAL -lPCILinuxBusAdapter -lxpci -lxcept -lxerces-c -lconfig -ltoolbox -llog4cplus -lasyncresolv -luuid -lpthread
#LIBS=-luuid -lpthread
LIBS=-lpthread

#INCDIR=-I/opt/xdaq/include/hal -I/opt/xdaq/include
#INCDIR=-I/opt/xdaq/include
#LIBDIR=-L/opt/xdaq/lib

#CFLAGS+=-W -Wall -Wconversion -O3 -std=c++11 -g
CFLAGS+=-W -Wall -Wconversion -O3 -std=c++17 -g
C99FLAGS+=-W -Wall -Wconversion -O3 -g


# -ftree-vectorizer-verbose=5 -ftree-vectorize
#vpath %.cc :../../common
default: a3818_bench

kernel.o : kernel.c kernel.h
	$(CC) $(C99FLAGS) -c $(INCDIR) $< -o $@

a3818.o : a3818.c a3818.h
	$(CC) $(C99FLAGS) -c $(INCDIR) $< -o $@

bench.o : a3818_bench.cc
	$(CXX) $(CFLAGS) -c $(INCDIR) $< -o $@
#	$(CXX) $(CFLAGS) -g -c -fverbose-asm -masm=intel -Wa,-ahl=bench.asm $(INCDIR) $< 

a3818_bench : a3818_bench.o kernel.o a3818.o
	$(CXX) $(CPLAGS) $(LIBDIR) $(LIBS) kernel.o a3818.o $< -o a3818_bench

#ldpath :
#	echo "export LD_LIBRARY_PATH=/opt/xdaq/lib" > ldpath

clean:
	rm -rf $(NAME) *.o a3818_bench
