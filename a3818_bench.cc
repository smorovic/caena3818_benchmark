
/*
 * a3818_bench.cc
 * 
 * CMS DAQ I/O Benchmarking tool.
 * 
 * By Petr Zejdl
 *
 */

//#define DO_CRATE_TEST

#if __GNUG__ < 8
#error "This software is written for C++17 and requires GCC 8"
#endif

#include <cassert>
#include <cstring>
#include <iostream>
#include <chrono>
#include <thread>
#include <sstream>
#include <iomanip>
#include <typeinfo>
#include <memory>
#include <vector>
#include <deque>
#include <numeric>
#include <functional>

//#include "hal/PCIAddressTable.hh"
//#include "hal/PCIAddressTableASCIIReader.hh"
//#include "hal/PCIDevice.hh"
//#include "hal/PCILinuxBusAdapter.hh"

#include <linux/ioctl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "a3818.h"
#include "kernel.h"

#define A3818_VENDORID      0x10EE //XILINX
#define A3818_DEVICEID      0x1015 //CAEN

//pattern TEST helper for VMECrate (from Evgueni)

class TestNum {
public:
  uint64_t nextnum;
  uint32_t patterns[5];
  TestNum() {
    nextnum = 0;
    patterns[0] = 0;
    patterns[1] = 0xffffffff;
    patterns[2] = 0x55555555;
    patterns[3] = 0xaaaaaaaa;
    patterns[4] = 0xcafecafe;

  }
  uint32_t getNext() {
    uint32_t part;
    int npart = nextnum%10;;
    if(npart%2 == 0)  part = patterns[npart/2];
    if(npart%2 == 1)  part = random();
    //if(npart%2 == 1)  part = 0xdddddddd;
    nextnum++;
    return part;
  }
};


/* Class for accessing devices through RAW I/O */
class RAW_Device {
public:
    RAW_Device()  {
        PCI_vaddr_ = NULL;
        
    }
    
    virtual ~RAW_Device() {}
      
    int open(uint32_t vendor_id, uint32_t device_id, a3818_state *s, uint32_t link_index, uint32_t barindex,uint32_t defaultOffset,std::string& devfile) {

	short index = 0;
	opt_link_ = link_index;
        std::cerr << "  VendorID " << std::hex << vendor_id << "   Device " << std::hex << device_id << "   Index " << index << std::endl;
	defaultOffset_ = defaultOffset;

        //fd_ = ::open(devfile.c_str(),O_RDWR);
        if (find_pci_resource(&pci_resource_, vendor_id, device_id, index) != 0) {
            std::cerr << "Cannot find the requested PCI device" << std::endl;
            //throw("Cannot find the requested PCI device");
	    assert(0);
            return -1;
        }
        
        PCI_size_ = pci_resource_.bar_size[barindex];
	std::cout << "    bar " << barindex << " size " << PCI_size_ << " " << std::hex << pci_resource_.bar_start[barindex] << std::endl;//DEBUG
        if ((PCI_vaddr_ = ioremap(pci_resource_.bar_start[barindex], PCI_size_)) == NULL) {
	    assert(0);
            return -1;
        }
	state_ = s;
        ::init_state(state_,link_index, (void*)PCI_vaddr_);

        return 0;
    }
        
    void close() {
        if (PCI_vaddr_ != 0) {
             iounmap(PCI_vaddr_, PCI_size_);
        }
    }
    
    uint32_t read32(uint32_t offset) {
        assert(PCI_vaddr_ != 0);
        //return in_32(PCI_vaddr_ + offset); //TODO - offset is in bytes or 32-bit units?
        return in_32(reinterpret_cast<void *>( reinterpret_cast<size_t>(PCI_vaddr_) + offset) );
    }

    uint64_t read64(uint32_t offset) {
        assert(PCI_vaddr_ != 0);
        return in_64(reinterpret_cast<void *>( reinterpret_cast<size_t>(PCI_vaddr_) + offset) );
    }

    void write32(uint32_t offset, uint32_t data) {
        assert(PCI_vaddr_ != 0);
        out_32(data, reinterpret_cast<void *>( reinterpret_cast<size_t>(PCI_vaddr_) + offset) );
    }

    void write64(uint32_t offset, uint64_t data) {
        assert(PCI_vaddr_ != 0);		
        out_64(data, reinterpret_cast<void *>( reinterpret_cast<size_t>(PCI_vaddr_) + offset) );
    }
    uint32_t getDefaultOffset() const {return defaultOffset_;}
    
private:
    volatile void *PCI_vaddr_;		/* PCI virtual address */
    size_t PCI_size_;			/* PCI address space size */
    //char *default_item_;
    uint32_t defaultOffset_;
    pci_resource pci_resource_;
    int fd_=-1;
public:
    struct a3818_state *state_;
    int opt_link_;
    
};

/******************************************************************************
 * Benchmark functions
 *****************************************************************************/

//#define WRTO A3818_IOCTL_S
//#define WRTO A3818_SSRAM_AD
#define WRTO A3818_DEBUG
#define WRVAL 1

template<typename Device>
void test_write32_pci(Device& dev, size_t size)
{
 
    for (unsigned int m = 0; m < size ; m++)
      ioctl_reg_writel(dev.opt_link_, dev.state_, WRTO, WRVAL);
}

template<typename Device>
void test_read32_pci(Device& dev, size_t size)
{
 
    for (unsigned int m = 0; m < size ; m++)
      ioctl_reg_readl(dev.opt_link_, dev.state_, WRTO);
}


template<typename Device>
void test_combined32_pci(Device& dev, size_t size)
{
 
    for (unsigned int m = 0; m < size ; m++) {
      ioctl_reg_writel(dev.opt_link_, dev.state_, WRTO, WRVAL);
                        asm volatile("": : :"memory"); //needs a barrier here, otherwise trs value never gets correctly updated
      ioctl_reg_readl(dev.opt_link_, dev.state_, WRTO);
    }
}

/*
#ifdef DO_CRATE_TEST
constexpr uint32_t VAL_POS_W = 6; 

template<typename Device>
void test_write32(Device& dev, size_t size)
{
    int ret;
    uint32_t value,valueout;

    TestNum pattern;
    struct a3818_comm comm_in;
    char outbuf[64];
    char inbuf[64];
    comm_in.out_buf = outbuf;
    comm_in.in_buf = inbuf;
    int commst;
    comm_in.status = &commst;
    

    for (unsigned int m = 0; m < size ; m++) {
	    //write
            value = pattern.getNext();

            comm_in.out_count = 10;//?10 bytes? (6 for read?)
            comm_in.in_count = 64;

	    outbuf[0]=0xa1;
	    outbuf[1]=0x89;
	    outbuf[2]=0x20;
	    outbuf[3]=0x43;
	    if (dev.opt_link_==1) //?
	      outbuf[3]=0x83;
	    outbuf[4]=0xff;
	    outbuf[5]=0xf3;
            for (int j=0;j<64;j++) inbuf[j]=0;

	    *((uint32_t*)(comm_in.out_buf+VAL_POS_W))=value;

	    *comm_in.status = 0;
            ret = ioctl_comm(dev.opt_link_, dev.state_, &comm_in);
	    assert(ret==0);

    }

}


template<typename Device>
void test_read32(Device& dev, size_t size)
{
    int ret;
    uint32_t value,valueout;

    struct a3818_comm comm_in;
    char outbuf[64];
    char inbuf[64];
    comm_in.out_buf = outbuf;
    comm_in.in_buf = inbuf;
    int commst;
    comm_in.status = &commst;
    

    for (unsigned int m = 0; m < size ; m++) {
            //read
            comm_in.out_count = 6;
            comm_in.in_count = 64;//even for read..
	
	    outbuf[0]=0xa1;
	    outbuf[1]=0xc9;
	    outbuf[2]=0x20;
	    outbuf[3]=0x43;
	    if (dev.opt_link_==1)
	      outbuf[3]=0x83;
	    outbuf[4]=0xff;
	    outbuf[5]=0xf3;
            for (int j=0;j<64;j++) inbuf[j]=0;

	    *comm_in.status = 0;
            ret = ioctl_comm(dev.opt_link_, dev.state_, &comm_in);
	    assert(ret==0);
    }
}




template<typename Device>
void test_combined32(Device& dev, size_t size)
{
    int ret;
    uint32_t value,valueout;

    TestNum pattern;
    struct a3818_comm comm_in;
    char outbuf[64];
    char inbuf[64];
    comm_in.out_buf = outbuf;
    comm_in.in_buf = inbuf;
    int commst;
    comm_in.status = &commst;
    

    for (unsigned int m = 0; m < size ; m++) {
	    //write
            value = pattern.getNext();

            comm_in.out_count = 10;
            comm_in.in_count = 64;

	    outbuf[0]=0xa1;
	    outbuf[1]=0x89;
	    outbuf[2]=0x20;
	    outbuf[3]=0x43;
	    if (dev.opt_link_==1) //?
	      outbuf[3]=0x83;
	    outbuf[4]=0xff;
	    outbuf[5]=0xf3;
            for (int j=0;j<64;j++) inbuf[j]=0;

	    *((uint32_t*)(comm_in.out_buf+VAL_POS_W))=value;

	    *comm_in.status = 0;
            ret = ioctl_comm(dev.opt_link_, dev.state_, &comm_in);
	    assert(ret==0);

            //read
            comm_in.out_count = 6;
            comm_in.in_count = 64;
	
	    outbuf[0]=0xa1;
	    outbuf[1]=0xc9;
	    outbuf[2]=0x20;
	    outbuf[3]=0x43;
	    if (dev.opt_link_==1)
	      outbuf[3]=0x83;
	    outbuf[4]=0xff;
	    outbuf[5]=0xf3;
            for (int j=0;j<64;j++) inbuf[j]=0;

	    *comm_in.status = 0;
            ret = ioctl_comm(dev.opt_link_, dev.state_, &comm_in);
	    assert(ret==0);
            //check if read value is the same
            assert(value == ((uint32_t*)(comm_in.in_buf))[0]);
    }
}
#endif
*/

/******************************************************************************
 * Benchmark tools
 *****************************************************************************/

/*
 * This will time execution of function F and returns time in seconds.
 */
template<typename F, typename... Args>
double timeExecution(F func, Args&&... args)
{
    auto start = std::chrono::high_resolution_clock::now();
    func(std::forward<Args>(args)...);
    auto end = std::chrono::high_resolution_clock::now();

    std::chrono::duration<double> diff = end - start;
    return diff.count();
}

/*
 * p
 * Calculates throughput in Cycles/ms and returns it as a std::string.
 */
std::string getThroughputCycles(size_t volume, double time)
{
    double throughput = static_cast<double>(volume) / time ; // double{1<<20};

    std::ostringstream ss;
    ss << std::setw(4) << (int)(throughput/1000) << " cycles/msec";
    return ss.str();
}


/*
 * Calculates throughput in MB/s and returns it as a std::string.
 */
std::string getThroughput(size_t volume, double time)
{
    //decimal MB/sec
    double throughput = 4.* static_cast<double>(volume) / time /1000.; // double{1<<20};

    std::ostringstream ss;
    ss << std::setw(8) << std::fixed << std::setprecision(2) << throughput << " MB/sec";
    return ss.str();
}



/******************************************************************************
 * Benchmark management
 *****************************************************************************/

/*
 * Runs a function in multiple threads over collection of hardware devices. 
 */
template<typename Devices, typename Function>
void runSingleTest(std::string description, size_t testMemorySize, Devices& devices, size_t nbThreads, Function function)
{
    // Where to collect results
    std::vector<double> collectedExecutionTimes(nbThreads);

    // Collection of threads
    std::vector<std::thread> threads;
    threads.reserve(nbThreads);

    // ------------------------------------------------------------------------
    // Spawn N= threads for concurent access (to different boards)
    for (size_t i = 0; i < nbThreads; i++) {
        auto& device = devices[i];
        double& executionTime = collectedExecutionTimes[i];

        // Use a lambda function to run the test in a thread
        threads.emplace_back([&device, &executionTime, &function, &testMemorySize]
        {
            executionTime = timeExecution( function, device, testMemorySize );
        });
    }
    // ------------------------------------------------------------------------

    // Wait for all threads to finish their job
    for (auto& t : threads) {
        t.join();
    }

    // Calculate average and print results
    double sum = std::accumulate(collectedExecutionTimes.begin(), collectedExecutionTimes.end(), 0.0);
    double avg = sum / static_cast<double>(nbThreads);

    std::cout << description << " throughput(max): " << getThroughputCycles(testMemorySize*nbThreads, avg) << "("<< getThroughput(testMemorySize*nbThreads, avg) <<")"<< "\n";
    std::cout << description << " throughput(avg): " << getThroughputCycles(testMemorySize, avg) << " [";
    for (auto result : collectedExecutionTimes) {
        std::cout << getThroughputCycles(testMemorySize, result) << ' ';
    }
    std::cout << " ] time: " << std::setfill(' ') << std::setw(8) << std::fixed << std::setprecision(4) << avg << " s" << std::endl;
}

/*
 * Runs a scaling tests 
 */
template<typename Devices, typename Function>
void runScalingTest(std::string description, size_t testMemorySize, Devices& devices, size_t nbThreads, Function function)
{
    // for (size_t threads = (runScalingTests ? 1 : nbThreads); threads <= nbThreads; threads++) {
    //     runSingleTest(std::to_string(threads) + "x " + description, testMemorySize, devices, threads, function);
    // }
    runSingleTest(std::to_string(nbThreads) + "x " + description, testMemorySize, devices, nbThreads, function);
}

/*
 * Run all tests
 */
template<typename Device, typename Devices>
void runTests(std::string description, size_t testMemorySize, size_t nbThreads, Devices& devices)
{

/* Note: these tests emulate CAEN Librady and Driver in userspace, i.e. full handshake to read words from VME crate.
 * On average it needs about 10 I/O operations for one write or read and is 10x slower.
 * It also requires the card to be initialized before (using other CAEN software), which disables IRQ (otherwise it interferes with the test) */

//    runScalingTest(description + "write32 ", testMemorySize, devices, nbThreads, test_write32<Device>);
//    runScalingTest(description + "read32  ", testMemorySize, devices, nbThreads, test_read32<Device>);
//    runScalingTest(description + "combo32 ", testMemorySize, devices, nbThreads, test_combined32<Device>);

/* Note: these tests are simple read, write or combinnation using DEBUG register in A3818 card.
 * This test already demonstrates the problem and only requires card to be plugged into the PCI slot
 * */
    runScalingTest(description + "write32_pci ", testMemorySize, devices, nbThreads, test_write32_pci<Device>);
    runScalingTest(description + "read32_pci ", testMemorySize, devices, nbThreads, test_read32_pci<Device>);
    runScalingTest(description + "combo32_pci ", testMemorySize, devices, nbThreads, test_combined32_pci<Device>);
}


/******************************************************************************
 * Some additional functions, tests
 *****************************************************************************/

/*
 * Run a test over specified clock class and prints resolution.
 */
#define printClockResolution(Clock)     printClockResolution_<Clock>(#Clock)

template <typename Clock>
void printClockResolution_(std::string typeName)
{
    std::cout << "Testing clock: " << typeName << '\n';
    std::cout << "  Type precision: " << Clock::period::num << "/" << Clock::period::den << " second " << '\n';
    std::cout << "  Is steady?    : " << (Clock::is_steady ? "Yes" : "No") << std::endl;
    std::cout << "  Sleeping for 1 second...";

    auto start = Clock::now();
    std::this_thread::sleep_for( std::chrono::microseconds(1000000) );
    auto end = Clock::now();

    std::chrono::duration<double> diff = end - start;
    std::cout << ' ' << std::setw(11) << std::fixed << std::setprecision(9) << diff.count() << " seconds elapsed" << std::endl;
}

void runClockTest()
{
    printClockResolution(std::chrono::high_resolution_clock);
    std::cout << std::endl;
    printClockResolution(std::chrono::system_clock);
    std::cout << std::endl;
    printClockResolution(std::chrono::steady_clock);
}

/******************************************************************************
 * Main
 *****************************************************************************/

void printHelp(char *argv0)
{
    std::cout << "Usage: " << argv0 << " Size_in_MB [Threads] [[Scaling (0/1)]] \n";
}

int main(int argc, char *argv[])
{
    size_t optionNbThreads = 1; //=links
    size_t optionStartFrom = 0;
    size_t optionCycles = 1000000;

    if (argc<=1) {
      std::cout << "Requires parameters: number of threads (1-3), number of cycles per thread (default:1M), BAR index offset start (default:0)" << std::endl;
      exit(1);
    }

    optionNbThreads = std::stoul( argv[1] );
    assert(optionNbThreads<=3 && optionNbThreads>0);
    if (argc>2) optionCycles =  std::stoul( argv[2] );
    if (argc>3) optionStartFrom =  std::stoul( argv[3] );

    uint32_t slotId[] =  {6,112,206};
    uint32_t links[] = {0,1,2};
    uint32_t barMap[] = {0,1,2};

    uint32_t addrs[] = {0x320,0x320,0x320}; //crate address

    std::vector<std::string> deviceFiles = {"/dev/a3818_0","/dev/a3818_8","/dev/a3818_16"};
    size_t seq = optionStartFrom; //starting index

    std::vector<RAW_Device> mmapDevices;

    struct a3818_state state;
    memset(&state,0,sizeof(a3818_state));

    for (int i=0;i<optionNbThreads;i++) {
      seq=(i+optionStartFrom)%3;
      uint32_t bar = barMap[seq];
      uint32_t defaultAddr = addrs[seq];
      uint32_t link = links[seq];
      std::string &devfile = deviceFiles[seq];
      mmapDevices.emplace_back( RAW_Device() );
      std::cout << "opening  BAR " << bar << " access at: " << defaultAddr << std::endl;
      auto status = mmapDevices[mmapDevices.size()-1].open(A3818_VENDORID,A3818_DEVICEID,&state,link,bar,defaultAddr,devfile);
      assert(status==0);
    }

    std::cout << "MMAP Tests:" << std::endl;
    runTests<RAW_Device>("MMAP ", optionCycles,optionNbThreads, mmapDevices);

    std::cout << "main() finished.\n";

    return 0;
}

